import { createAppServer } from "cmd/rest/server";
import request from "supertest";

describe("filter router test", () => {    
    
    test("validation should be fail when model invalid", async () => {       
        
        const server = createAppServer({
            FindRecords:jest.fn()
        });
        const data = { 
            startDate: new Date('2017-01-20'), 
            endDate: new Date('2017-01-30'), 
            minCount: 1, 
            maxCount: 0 
        };        
        const res = await request(server).post("/filter").send(data);
        expect(res.statusCode).toEqual(400);
    });    
});
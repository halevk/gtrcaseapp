import { IRecordRepository } from "app/abstractions";
import { filterRecordsHandlerFactory, IFilterRecordRequest } from "app/handlers/filterRecordsHandler";

describe("filter record handler",()=> {
    test("should return success",async ()=> {
        const repo:IRecordRepository = {
            FindRecords:jest.fn().mockImplementation(()=> Promise.resolve([]))
        };
        var req:IFilterRecordRequest = {endDate:"",startDate:"",maxCount:0,minCount:10};
        var handler = filterRecordsHandlerFactory(repo);
        var resp = await handler(req);
        expect(resp.code).toEqual(0);
    });
});
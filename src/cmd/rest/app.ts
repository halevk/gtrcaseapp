import { IAppConfig, IRecordRepository } from "app/abstractions";
import { registerRepo } from "repo";
import { logger } from "./logger";
import { createAppServer } from "./server";


const startServer = async () => {
    const config: IAppConfig = {
        dbUrl: process.env.DB_URL || "",
        collectionName: process.env.COLLECTION_NAME || "records",
        dbName: process.env.DB_NAME || "getir-case-study",
        port: process.env.PORT || 5000
    };

    // register Repo
    const repo = await registerRepo(logger, config);
    if(repo){
        const app = createAppServer(repo);
        app.listen(config.port, () => logger.info(`server is runnin on port ${config.port}`));
    }  
}

startServer().catch(err => logger.error(`err:${err} happened on startup`));


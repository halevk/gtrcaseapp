import { NextFunction, Request, Response, Router } from "express"
import Joi from "joi";
import { IRecordFilter, IRecordRepository } from "app/abstractions"
import { HttpException } from "app/errors";
import { filterRecordsHandlerFactory } from "app/handlers/filterRecordsHandler"
import { errorMiddleware } from "../middlewares";

const schema = Joi.object<IRecordFilter>({
    startDate: Joi.date().less(Joi.ref('endDate')).required(),
    endDate: Joi.date().required(),
    minCount: Joi.number().integer().min(0).required(),
    maxCount: Joi.number().integer().greater(Joi.ref('minCount')).required()
});

const filterRecordsRouteValidator = 
     (req: Request,resp:Response,next:NextFunction):void => {
        const {value,error} =  schema.validate(req.body);
        if(error){
            var msg = error.details.map(p=> p.message)[0];
            return next(new HttpException(400,msg));
        }
        return next();    
}

const filterRecordsRoute = (repo: IRecordRepository) =>
    async (req: Request, resp: Response,next:NextFunction):Promise<void> => {       
        var request = req.body as IRecordFilter;        
        const handler = filterRecordsHandlerFactory(repo);
        try{
            var response = await handler(request);
            resp.send(response).status(200);
        }catch(err:any){
            resp.send({"code":1,"msg":err.message}).status(500);
        }        
    }

export const filterRoute = (repo: IRecordRepository) => {
    const router = Router(); 
    router.post("/",filterRecordsRouteValidator,errorMiddleware,filterRecordsRoute(repo))
    return router;
};

 
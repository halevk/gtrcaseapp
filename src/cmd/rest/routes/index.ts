import { Router} from "express";
import { IRecordRepository } from "app/abstractions";
import { filterRoute } from "./filter";

export const registerRoutes =(repo:IRecordRepository) => {
    const router = Router();
    router.use("/filter",filterRoute(repo));
    return router;
}
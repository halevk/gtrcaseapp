require("dotenv").config();
import { registerRoutes } from "./routes";
import { errorHandler, successHandler } from "./logger";
import { errorMiddleware, notFoundMiddleware } from "./middlewares";
import { IRecordRepository } from "app/abstractions";

import express,{ Application } from "express";

export const createAppServer = (repo: IRecordRepository) => {
    const app: Application = express();
    app.use(successHandler);
    app.use(errorHandler);
    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));        
    app.use(registerRoutes(repo));
    app.use(errorMiddleware);
    app.use(notFoundMiddleware);
    return app;
}

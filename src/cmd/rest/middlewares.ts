import { Request,Response, NextFunction } from "express";
import { HttpException } from "app/errors";

export function errorMiddleware(error: HttpException, request: Request, response: Response, next: NextFunction) {
    if(error){
        const code = error.status ;
        const msg = error.message ;
        response
          .status(code)
          .send({
            code,
            msg,
          });
          return;
    }
    next();
}

export const notFoundMiddleware = (req:Request,resp:Response) => {
  resp.status(404).send({code:404,msg:"requested endpoint not found"});
}
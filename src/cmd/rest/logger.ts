import { Request, Response } from "express";
import morgan from "morgan";
import winston from "winston";

const format = winston.format((msg) => {
    if (msg instanceof Error) {
      Object.assign(msg, { message: msg.stack });
    }
    return msg;
  });
  
  export const logger = winston.createLogger({
    level: 'info',
    format: winston.format.combine(
      format(),
      winston.format.colorize(),
      winston.format.splat(),
      winston.format.printf(({ level, message }) => `${level}: ${message}`)
    ),
    transports: [
      new winston.transports.Console({
        stderrLevels: ['error'],
      }),
    ],
  });


morgan.token('message', (req:Request, res:Response) => res.locals.errorMessage || '');

const successResFormat = `$:method :url :status - :response-time ms`;
const errorResFormat = `$:method :url :status - :response-time ms - message: :message`;

export const successHandler = morgan(successResFormat, {
  skip: (req:Request, res:Response) => res.statusCode >= 400,
  stream: { write: (message) => logger.info(message.trim()) },
});

export const errorHandler = morgan(errorResFormat, {
  skip: (req:Request, res:Response) => res.statusCode < 400,
  stream: { write: (message) => logger.error(message.trim()) },
});


import { MongoClient } from "mongodb";
import winston from "winston";
import { IAppConfig, IRecordRepository } from "app/abstractions";
import { MongoRecordRepository } from "./mongoRepository";

export const registerRepo = async (logger:winston.Logger,config:IAppConfig): Promise<IRecordRepository|null> => {      
    let client:MongoClient | null = null;
    try{
        client = await new MongoClient(config.dbUrl).connect();
        logger.info("db connection established");
        return new MongoRecordRepository(client, config.dbName, config.collectionName);
    }catch(err:any){
        logger.error(`db connection error: ${err.message}`);
    }
    return null;
}
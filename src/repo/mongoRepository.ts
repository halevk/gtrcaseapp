import { Collection, MongoClient } from "mongodb";
import { IRecordData, IRecordFilter, IRecordRepository } from "app/abstractions";

export class MongoRecordRepository implements IRecordRepository {   

    constructor(public _client:MongoClient,
                public _dbName:string,
                public _collectionName:string){       
    }

    GetCollection<T>():Collection<T>{   
        return this._client.db(this._dbName).collection<T>(this._collectionName);
    }

    async FindRecords(filter: IRecordFilter): Promise<IRecordData[]> {               
        let collection = this.GetCollection<IRecordData>();        
        let query = [
            { "$match": { "createdAt": { "$gte": new Date(filter.startDate), "$lt": new Date(filter.endDate) } } },
            { "$project": { "_id": false, "key": "$key", "createdAt": "$createdAt", "totalCount": { "$sum": "$counts" } } },
            { "$match": { "totalCount": { "$gte": filter.minCount, "$lte": filter.maxCount } } }
        ];        
        return await collection.aggregate<IRecordData>(query).toArray();
    }

}
export interface IRecordRepository {
    FindRecords(filter:IRecordFilter) : Promise<IRecordData[]>
 }
 
 export interface IRecordData {
     createdAt:string,
     key:string,    
     totalCount:number
 }
 
 export interface IRecordFilter{
     startDate:string,
     endDate:string,
     minCount:number,
     maxCount:number
 }

 export interface IAppConfig {
     dbUrl:string,
     dbName:string,
     collectionName:string,
     port:number|string
 }
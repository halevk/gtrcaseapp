import { IRecordRepository } from "app/abstractions";
import { IResponseBase } from "app/models";

export interface IFilterRecordRequest {
    startDate:string,
    endDate:string,
    minCount:number,
    maxCount:number
}

export interface IFilterRecordResponse extends IResponseBase {
    records:IFilterRecordItem[]
}

export interface IFilterRecordItem {
    key:string,
    createdAt:string,
    totalCount:number
}

export const filterRecordsHandlerFactory=(repo:IRecordRepository) => 
    async (req:IFilterRecordRequest):Promise<IFilterRecordResponse> => {        
        var records = await repo.FindRecords({
            startDate:req.startDate,
            endDate:req.endDate,
            minCount:req.minCount,
            maxCount:req.maxCount});           
        return {code:0,msg:"success",records:records};
    }
# Test Case API

Application build with the following; NodeJS with Typescript ,Express and Mongo 

## API is deployed to Heroku

```bash
curl --location --request POST 'https://gtrtestcaseapp.herokuapp.com/filter' \
--header 'Content-Type: application/json' \
--data-raw '{
    "startDate": "2016-01-26",
    "endDate": "2018-02-02",
    "minCount": 2700,
    "maxCount": 3000
}'
```

## Installation

Pull the repo:

```bash
git clone https://gitlab.com/halevk/gtrcaseapp.git
cd gtrcaseapp
```

```bash
npm install 
```

```bash
cp .env.example .env

# open .env and modify the environment variables
```

## Commands

Local running:

```bash
npm run dev
```

Running for prod:

```bash
npm start
```

Testing:

```bash
# run all tests
npm test

# run test coverage
npm run coverage
```

## Environment Variables

```bash
# Port number
PORT=5500

# URL of the Mongo DB
MONGODB_URI=mongodb://127.0.0.1:27017/getir
```

## App Structure

```
src\
 |--app\            # main app folder     
 |-- cmd\rest\      # rest hosting app which is express
 |-- repo\          # infrastructure repo
```

## Endpoints

List of available routes:

**Record routes**:\
`POST /filter` - For filtering records\

## Error Handling

Error handling is provided by express middlewares, with the help of **"notFoundMiddleware"** and **"errorMiddleware"** located in cmd-> rest-> middlewares.js.

Unified response structure is kept for exception handling as below;

```json
{
  "code": 404,
  "msg": "requested endpoint not found"
}
```
or

```json
{
    "code": 400,
    "msg": "\"maxCount\" must be greater than ref:minCount"
}
```

## Validation & Loggind

For model validation : [Joi](https://joi.dev/) library has used.
For logging : [Winston](https://github.com/winstonjs/winston) library has used.
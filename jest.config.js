/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  coverageProvider:"v8",
  moduleFileExtensions:["js","jsx","ts","tsx","json","node"],
  roots:["<rootDir>/src"],
  testMatch :["**/src/test/**/*.[jt]s?(x)","**/?(*.)+(spec|test).[tj]s?(x)"],
  transform:{
    "^.+\\.(ts|tsx)$":"ts-jest"
  },
  moduleDirectories:["node_modules","src"]
};